<?php
return [
    'image_dimensions' => [
        'slider' => [
            'image' => [
                [
                    'width' => 1140,
                    'height' => 370
                ],
            ],
        ],
        'subcategory' => [
            'image' => [
                [
                    'width' => 850,
                    'height' => 350
                ],
            ],
        ],

        'user' => [
            'image' => [
                [
                    'width' => 160,
                    'height' => 160
                ],
            ],
        ],

        'product' => [
            'image' => [
                [
                    'width' => 260,
                    'height' => 260
                ],
                [
                    'width' => 150,
                    'height' => 150
                ],
            ],
        ],

        'service' => [
            'image' => [
                [
                    'width' => 825,
                    'height' => 430
                ],
            ],
        ],

        'client' => [
            'image' => [
                [
                    'width' => 97,
                    'height' => 97
                ],
            ],
        ],

        'advertisement' => [
            'image' => [
                [
                    'width' => 1140,
                    'height' => 100
                ],
                [
                    'width' => 260,
                    'height' => 220
                ],
            ],
        ],

        'news' => [
            'image' => [
                [
                    'width' => 825,
                    'height' => 525
                ],
                [
                    'width' => 825,
                    'height' => 430
                ],
            ],
        ],

        'setting' => [
            'sitelogo' => [
                [
                    'width' => 145,
                    'height' => 124
                ],
            ],
            'ceoimage' => [
                [
                    'width' => 100,
                    'height' => 100
                ],
            ],
        ],

    ],
];
