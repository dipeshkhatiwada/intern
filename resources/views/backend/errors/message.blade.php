<!DOCTYPE html>

@include('backend.includes.head')

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    @include('backend.includes.header')

    <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        {{--                    <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">--}}
                        <img src="@auth{{asset('images/users/'.Auth::user()->image)}}@endauth" class="img-circle" alt="User Image">
                        {{--@auth<a href="{{asset('images/users/'.Auth::user()->image)}}">dsf</a>@endauth--}}
                    </div>
                    <div class="pull-left info">
                        <p>@auth{{ Auth::user()->name }}@endauth</p>
                        <a href=""><i class="fa fa-circle text-success"></i> Online</a>
                        <a class="" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                            ( {{ __('Logout') }})
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

                </ul>

            </section>
            <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    {{$code}}  Error Page
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">{{$code}}  error</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="error-page">
                    <h2 class="headline text-yellow"> {{$code}} </h2>

                    <div class="error-content">
                        <h1><i class="fa fa-warning text-yellow"></i> <h3> Oops! {{!empty($message)?$message:'There was some error.'}} </h3> </h1>
                        <hr/>

                        <p>
                            We couldn't find the problem in the page you were looking for.
                            Meanwhile, you may <h5> <a href="{{route('home')}}">Return to dashboard</a></h5>
                        </p>


                    </div>
                    <!-- /.error-content -->
                </div>
                <!-- /.error-page -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

    {{--@include('backend.includes.menu')--}}
    {{--@yield('content')--}}

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; {{\Carbon\Carbon::now()->year}} <a href="http://dipeshkhatiwada.com.np">Dipesh Khatiwada </a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('backend/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('backend/dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<!-- Select2 -->
<script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<!-- CK Editor -->
<script src="{{asset('backend/bower_components/ckeditor/ckeditor.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- InputMask -->
<script>

    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>




</body>
</html>



{{--<div class="d-flex justify-content-center align-items-center" id="main">--}}
    {{--<h1 class="mr-3 pr-3 align-top border-right inline-block align-content-center">{{$code}} Error</h1>--}}
    {{--<div class="inline-block align-middle">--}}
        {{--<h2 class="font-weight-normal lead" id="desc">{{$message}}</h2>--}}
    {{--</div>--}}
