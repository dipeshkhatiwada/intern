
<div class="form-group">
        {{ Form::label('name', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
        {{ Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter name']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
</div>
<div class="form-group">
    {{ Form::label('title', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::text('title', null, ['class' => 'form-control','placeholder'=>'Enter title']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'title'])
</div>
<div class="form-group">
    {{ Form::label('slug', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::text('slug', null, ['class' => 'form-control','placeholder'=>'Enter slug']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'slug'])
</div>
<div class="form-group">
    {{ Form::label('rank', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::number('rank', null, ['class' => 'form-control','placeholder'=>'Enter rank']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'rank'])

</div>
<div class="form-group">
    {{ Form::label('description', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::textarea('description', null, ['class' => 'form-control','placeholder'=>'Enter description',"id"=>"my-editor"]) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'description'])
</div>

