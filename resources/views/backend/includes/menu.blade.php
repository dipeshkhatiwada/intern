<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                {{--                    <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">--}}
                <img src="@auth{{asset('images/users/'.Auth::user()->image)}}@endauth" class="img-circle" alt="User Image">
                {{--@auth<a href="{{asset('images/users/'.Auth::user()->image)}}">dsf</a>@endauth--}}
            </div>
            <div class="pull-left info">
                <p>@auth{{ Auth::user()->name }}@endauth</p>
                <a href=""><i class="fa fa-circle text-success"></i> Online</a>
                <a class="" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                    ( {{ __('Logout') }})
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            @php($permission_list = array_column(auth()->user()->role->perm->toArray(),'permission_id')))
            @php($module_list = array_column(auth()->user()->role->mod->toArray(),'module_id')))
            @foreach(auth()->user()->role->modules()->orderBy('rank','ASC')->get() as $module)
                @if(count($module->permissions->where('menu_display',1)) > 0  )
                    @if(count(array_diff($permission_list,$module->permissions->where('menu_display',1)->pluck('id')->toArray())) != count($permission_list))
                        <li class="treeview {!! request()->is($module->url)?'active menu-open':"" !!}">
                            <a href="#">
                                <i class="{{$module->icon}}"></i>
                                <span>{{$module->name}}</span>
                                <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                            </a>
                            <ul class="treeview-menu" style="display: {!! request()->is($module->url)?'block':'none' !!}" >
{{--                                {{dd($module->permissions->where('menu_display',1))}}--}}
                                @foreach($module->permissions()->where('menu_display',1)->orderBy('rank','ASC')->get() as $permission)
                                    @if(in_array($permission->id,$permission_list))
                                        <li class="{!! request()->is('backend/'.$permission->url.'*')?'active menu-open':"" !!}"><a href="{{route('backend.' . $permission->route)}}"><i class="{{$permission->icon}}"></i> {{$permission->name}}</a></li>
                                    @endif
                                @endforeach
                            </ul>

                        </li>
                    @endif
                @endif
            @endforeach
        </ul>
        {{--<ul class="sidebar-menu" data-widget="tree">--}}
        {{--<li class="header">MAIN NAVIGATION</li>--}}
        {{--<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>--}}
        {{--@php($permission_list = array_column(auth()->user()->role->perm->toArray(),'permission_id')))--}}
        {{--@php($module_list = array_column(auth()->user()->role->mod->toArray(),'module_id')))--}}
        {{--@foreach(auth()->user()->role->modules()->orderBy('rank','ASC')->get() as $module)--}}
        {{--@if(count($module->permissions->where('menu_display',1)) > 0  )--}}
        {{--@if(count(array_diff($permission_list,$module->permissions->where('menu_display',1)->pluck('id')->toArray())) != count($permission_list))--}}
        {{--<li class="treeview">--}}
        {{--<a href="#">--}}
        {{--<i class="fa fa-newspaper-o"></i>--}}
        {{--<span>{{$module->name}}</span>--}}
        {{--<span class="pull-right-container">--}}
        {{--<i class="fa fa-angle-left pull-right"></i>--}}
        {{--</span>--}}
        {{--</a>--}}
        {{--<ul class="treeview-menu" >--}}
        {{--@foreach($module->permissions->where('menu_display',1) as $permission)--}}
        {{--@if(in_array($permission->id,$permission_list))--}}
        {{--<li><a href="{{route('backend.' . $permission->route)}}"><i class="fa fa-circle-o"></i> {{$permission->name}}</a></li>--}}
        {{--@endif--}}
        {{--@endforeach--}}
        {{--</ul>--}}

        {{--</li>--}}
        {{--@endif--}}
        {{--@endif--}}
        {{--@endforeach--}}
        {{--</ul>--}}
    </section>
    <!-- /.sidebar -->
</aside>