@include('backend.includes.preview_image')
<div class="form-group">
    {{ Form::label('image', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::file('photo') }}
        @include('backend.includes.form_fields_validation',['fieldname' => 'photo'])

</div>


<div class="form-group">
    {{ Form::label('Status', null, ['class' => 'control-label ']) }}<br>
    {{ Form::radio('status', '1', ['class' => 'form-control'])  }} Active
    {{ Form::radio('status', '0', true)  }} De Active
        @include('backend.includes.form_fields_validation',['fieldname' => 'status'])

</div>