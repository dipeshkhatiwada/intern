<div class="form-group">
    {{ Form::label('category_id', 'Category', ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{--        {{ Form::text('category_id', null, (['class' => 'form-control'])) }}--}}
    {{ Form::select('category_id', $data['categories'],null, ['class' => 'form-control select2','placeholder'=>'Select category']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'category_id'])
</div>
<div class="form-group">
        {{ Form::label('name', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
        {{ Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter name']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
</div>
<div class="form-group">
    {{ Form::label('slug', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::text('slug', null, ['class' => 'form-control','placeholder'=>'Enter slug']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'slug'])
</div>
<div class="form-group">
    {{ Form::label('rank', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::number('rank', null, ['class' => 'form-control','placeholder'=>'Enter rank']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'rank'])

</div>
{{--<div class="form-group">--}}
    {{--{{ Form::label('image', null, ['class' => 'control-label']) }}--}}
    {{--{{ Form::file('file') }}--}}
    {{--@if($errors->has('file'))--}}
        {{--<label class="text text-danger">{{$errors->first('file')}}</label>--}}
    {{--@endif--}}
{{--</div>--}}
<div class="form-group">
    {{ Form::label('description', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::textarea('description', null, ['class' => 'form-control','placeholder'=>'Enter description',"id"=>"my-editor"]) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'description'])
</div>
<div class="form-group">
    {{ Form::label('meta_title', ' Meta Title (SEO)', ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::text('meta_title', null, ['class' => 'form-control','placeholder'=>'Enter meta title']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'meta_title'])
</div>
<div class="form-group">
    {!! Form::label('meta_keywords ', 'Meta Keywords (SEO) ') !!}<span class="text text-danger">*</span>
    {!! Form::textarea('meta_keywords',null, ["placeholder" => "Max 255 characters", "class" => "form-control","rows"=>4]) !!}
    @include('backend.includes.form_fields_validation',['fieldname' => 'meta_keywords'])
</div>
<div class="form-group">
    {!! Form::label('meta_description', 'Meta description (SEO)') !!}<span class="text text-danger">*</span>
    {!! Form::textarea('meta_description',null, ["placeholder" => "Max 300 characters", "class" => "form-control","rows"=>4]) !!}
    @include('backend.includes.form_fields_validation',['fieldname' => 'meta_description'])
</div>
