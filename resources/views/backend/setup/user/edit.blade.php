@extends('backend.layouts.app')

@section('content')

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$data['page_title']}}</h3>
                </div>
                {!! Form::model($data['user'], ['route' => ['backend.user.update', $data['user']->id],'files'=>true]) !!}
                {{ Form::hidden('_method', 'PUT') }}
                <div class="box-body">
                    @include('backend.user.partials.mainform')
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {!! Form::submit('Save !',['class'=> 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}

            <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->

@endsection
