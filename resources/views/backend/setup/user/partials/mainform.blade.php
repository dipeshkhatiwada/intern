 <div class="form-group">
    {{ Form::label('role_id', 'Role name', ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{--        {{ Form::text('category_id', null, (['class' => 'form-control'])) }}--}}
    {{ Form::select('role_id', $data['roles'],null, ['class' => 'form-control select2']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'status'])

</div>
<div class="form-group">
    {{ Form::label('name', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter name']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'name'])

</div>

<div class="form-group">
    {{ Form::label('email', null, ['class' => 'control-label']) }}<span class="text text-danger">*</span>
    {{ Form::email('email', null, ['class' => 'form-control','placeholder'=>'Enter email']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'email'])

</div>
<div class="form-group">
    {!! Form::label('phone', 'Phone') !!}<span class="text text-danger">*</span>
    {!! Form::text('phone',null, ["placeholder" => "Enter Phone", "class" => "form-control"]) !!}
    @include('backend.includes.form_fields_validation',['fieldname' => 'phone'])
</div>
<div class="form-group">
    {!! Form::label('password', 'Password') !!}<span class="text text-danger">*</span>
    {!! Form::password('password', ["placeholder" => "Enter Password", "class" => "form-control"]) !!}
    @include('backend.includes.form_fields_validation',['fieldname' => 'password'])
</div>
<div class="form-group">
    {!! Form::label('password_confirmation', 'Confirm Password') !!}<span class="text text-danger">*</span>
    {!! Form::password('password_confirmation', ["placeholder" => "Enter Confirm Password", "class" => "form-control"]) !!}
    @include('backend.includes.form_fields_validation',['fieldname' => 'password_confirmation'])
</div>