@extends('backend.layouts.app')
@section('title','Create '.$panel)
@section('content')
    <div class="content-wrapper">

        @include('backend.includes.breadcrumb',['page'=>'Assign Module'])

        <section class="content">
            @include('backend.includes.errors')
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$data['page_title']}}</h3>
                </div>

                {{--@include('backend.includes.error')--}}

                {!! Form::open(['route' => $base_route.'.postmodule', 'method' => 'post']) !!}
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('role_name', null, ['class' => 'control-label']) }}
                        {{ Form::text('role_name', $data['role']->name, (['class' => 'form-control','readonly' => 'true'])) }}

                        {{ Form::hidden('role_id', $data['role']->id) }}
                        @include('backend.includes.form_fields_validation',['fieldname' => 'role_name'])
                    </div>
                    <hr>
                    <div class="form-group">

                        {{ Form::label('module_id','Module Lists', ['class' => 'control-label']) }}
                        <br>
                        @foreach($data['modules'] as $module)
                            @if(in_array($module->id,$data['assigned_modules']))
                                {{ Form::checkbox('module_id[]', $module->id,true) }}{{$module->name}}<br/>
                            @else
                                {{ Form::checkbox('module_id[]', $module->id) }}{{$module->name}}<br/>

                            @endif

                        @endforeach
                        @include('backend.includes.form_fields_validation',['fieldname' => 'module_id'])

                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check icheck"></i>Save </button>
                    <button type="reset" class="btn btn-warning"><i class="fa fa-undo icheck"></i> cancel</button>
                </div>
                <!-- /.box-footer-->
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection

@section('js')
    @include('backend.includes.ckeditor')
    {{--@include('backend.includes.slug')--}}
@endsection
