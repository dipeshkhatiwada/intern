@extends('backend.layouts.app')
@section('title','Create '.$panel)
@section('content')
    <div class="content-wrapper">

        @include('backend.includes.breadcrumb',['page'=>'Assign Permission'])

        <section class="content">
            @include('backend.includes.errors')
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$data['page_title']}}</h3>
                </div>

                {{--@include('backend.includes.error')--}}

                {!! Form::open(['route' => $base_route.'.postpermission', 'method' => 'post']) !!}
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('role_name', null, ['class' => 'control-label']) }}
                        {{ Form::text('role_name', $data['role']->name, (['class' => 'form-control','readonly' => 'true'])) }}

                        {{ Form::hidden('role_id', $data['role']->id) }}

                        @if($errors->has('name'))
                            <label class="text text-danger"> {{$errors->first('name')}}</label>
                        @endif
                    </div>
                    <hr>
                    <div class="form-group">

                        {{ Form::label('permission_id','Permission Lists', ['class' => 'control-label']) }}
                        <br>
                        <ul>
                            @foreach($data['role']->modules as $module)
                                @if(count($module->permissions) >0)
                                    <li><strong>{{$module->name}}</strong></li>
                                    @foreach($module->permissions as $permission)

                                        @if(in_array($permission->id,$data['assigned_permissions']))
                                            {{ Form::checkbox('permission_id[]', $permission->id,true) }}{{$permission->name}}
                                        @else
                                            {{ Form::checkbox('permission_id[]', $permission->id) }}{{$permission->name}}

                                        @endif

                                    @endforeach

                                @endif
                            @endforeach
                        </ul>
                        @foreach($data['permissions'] as $permission)


                        @endforeach
                        @if($errors->has('name'))
                            <label class="text text-danger"> {{$errors->first('name')}}</label>

                        @endif
                    </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check icheck"></i>Save </button>
                    <button type="reset" class="btn btn-warning"><i class="fa fa-undo icheck"></i> cancel</button>
                </div>
                <!-- /.box-footer-->
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection

@section('js')
    @include('backend.includes.ckeditor')
    {{--@include('backend.includes.slug')--}}
@endsection
