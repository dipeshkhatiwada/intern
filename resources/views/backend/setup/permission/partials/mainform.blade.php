<div class="form-group">
    {{ Form::label('module_id', 'Module name', ['class' => 'control-label']) }}
    {{--        {{ Form::text('category_id', null, (['class' => 'form-control'])) }}--}}
    {{ Form::select('module_id', $data['modules'],null, ['class' => 'form-control select2','placeholder'=>'Select Module']) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'module_id'])
</div>
<div class="form-group">
        {{ Form::label('name', null, ['class' => 'control-label']) }}
        {{ Form::text('name', null, (['class' => 'form-control','placeholder'=>'Enter name'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
</div>
<div class="form-group">
    {{ Form::label('icon', null, ['class' => 'control-label']) }}
    {{ Form::text('icon', null, (['class' => 'form-control','placeholder'=>'Enter icon'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'icon'])
</div>
<div class="form-group">
        {{ Form::label('route', null, ['class' => 'control-label']) }}
        {{ Form::text('route', null, (['class' => 'form-control','placeholder'=>'Enter route'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'route'])
</div>
<div class="form-group">
        {{ Form::label('url', null, ['class' => 'control-label']) }}
        {{ Form::text('url', null, (['class' => 'form-control','placeholder'=>'Enter url'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'url'])
</div>
<div class="form-group">
    {{ Form::label('rank', null, ['class' => 'control-label']) }}
    {{ Form::number('rank', null, (['class' => 'form-control','placeholder'=>'Enter rank'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'rank'])
</div>




