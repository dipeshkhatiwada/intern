

<div class="form-group">
    {{ Form::label('menu_display', null, ['class' => 'control-label']) }}<br/>
    <label class="radio-inline"> {!! Form::radio('menu_display', 1, false) !!}Active </label>
    <label class="radio-inline"> {!! Form::radio('menu_display', 0, true) !!}Deactive </label>
    @include('backend.includes.form_fields_validation',['fieldname' => 'menu_display'])
</div>
<div class="form-group">
    {{ Form::label('Status', null, ['class' => 'control-label ']) }}<br/>
    <label class="radio-inline"> {!! Form::radio('status', 1, false) !!}Active </label>
    <label class="radio-inline"> {!! Form::radio('status', 0, true) !!}Deactive </label>
        @include('backend.includes.form_fields_validation',['fieldname' => 'status'])

</div>