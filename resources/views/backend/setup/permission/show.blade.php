@extends('backend.layouts.app')
@section('title','View '.$panel)
@section('css')
    <link rel="stylesheet"
          href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/css/alert.css')}}">
@endsection
@section('content')
    <div class="content-wrapper" style="min-height: 916px;">
    @include('backend.includes.breadcrumb',['page'=>'Details'])


    @include('backend.includes.message')
    <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details of {{ $panel }}</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid"
                                               aria-describedby="example1_info">
                                            @if($data['row']->count() == 0 )
                                                <tr>
                                                    <td class="bg bg-danger" colspan="2">Invalid Data</td>

                                                </tr>
                                            @else


                                                <tr>
                                                    <th width="25%">Title</th>
                                                    <td>{{$data['row']->title}}</td>
                                                </tr>

                                                <tr>
                                                    <th width="25%">Image Title </th>
                                                    <td>{{$data['row']->image_title }}</td>
                                                </tr>
                                                <tr>
                                                    <th width="25%">Meta Keyword </th>
                                                    <td>{{$data['row']->meta_keywords  }}</td>
                                                </tr>
                                                <tr>
                                                    <th width="25%">Meta Description </th>
                                                    <td>{{$data['row']->meta_description }}</td>
                                                </tr>

                                                <tr>
                                                <tr>
                                                    <th width="25%">Short Description</th>
                                                    <td>{!! $data['row']->short_description!!}</td>
                                                </tr>

                                                <tr>
                                                    <th width="25%">long Description</th>
                                                    <td>{!! $data['row']->long_description!!}</td>
                                                </tr>
                                                <tr>
                                                    <th width="25%">image </th>
                                                    <td><img src="{{asset('files/page').'/'. $data['row']->image}}" height="100" alt="{{$data['row']->image}}"/></td>
                                                </tr>
                                                <tr>
                                                    <th width="25%">Status</th>
                                                    <td>@if ($data['row']->status==0)
                                                            <span class="label label-warning">Deactive</span>
                                                        @else <span class="label label-success">Active</span>
                                                        @endif</td>
                                                </tr>
                                                <tr>
                                                    <th>Created At</th>
                                                    <td>{{ date('D, j M Y', strtotime($data['row']->created_at)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Updated At</th>
                                                    <td>{{ date('D, j M Y', strtotime($data['row']->updated_at)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Created By</th>
                                                    <td>{{Auth::user($data['row']->created_by)->name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Updated By</th>
                                                    <td>{{Auth::user($data['row']->updated_by)->name}}</td>
                                                </tr>
                                                <tr>

                                                    <td><a class="cust_btm btn btn-block btn-warning"
                                                           href="{{ route($base_route.'.edit', ['id' => $data['row']->id]) }}"><i
                                                                class="glyphicon glyphicon-edit"></i>Edit</a></td>

                                                    <td> <button href="" class=" btn btn-danger" data-toggle="modal" data-target="#myModal">
                                                            <i class="fa fa-trash"></i>Delete</button>

                                                    </td>
                                                    <div class="modal fade" id="myModal" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header  bg bg-primary">
                                                                    <h3 class="modal-title"> Delete Conformation</h3>
                                                                    <button type="button" class="close" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></button>
                                                                </div>
                                                                {!! Form::open(['route' => [$base_route.'.destroy', $data['row']->id],'method'=>'POST']) !!}
                                                                {{ Form::hidden('_method', 'DELETE') }}

                                                                <div class="modal-body">
                                                                    <h3>Are You Sure want to delete this record?</h3>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-danger" name="btnCreate"> <i class="fa fa-trash"></i> Delete ! </button>
                                                                    {{--{!! Form::submit("Delete !",['class'=> 'btn btn-danger']) !!}--}}
                                                                    {!! Form::close() !!}
                                                                    <button type="button" class="btn btn-warning" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> Cancel ! </button>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <script src="{{ asset('backend/js/general.js') }}"></script>
                                                </tr>

                                            @endif

                                        </table>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/alert.min.js') }}"></script>
    <script src="{{ asset('backend/js/alert.custom.js') }}"></script>
    <script src="{{ asset('backend/js/general.js') }}"></script>
    <script>
        $(function() {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })

        function handleTypeDelete(event) {
            event.preventDefault();
            handleDelete(event, '/backend/user_management/management/action/', "यो डाटा अन्य टेबलमा समेत प्रयोग भएको छ ।", "तपाई यो डाटा पुन: रिकभर गर्न सक्नु हुन्न !");
        }

    </script>
@endsection
