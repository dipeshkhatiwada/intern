<div class="form-group">
        {{ Form::label('name', null, ['class' => 'control-label']) }}
        {{ Form::text('name', null, (['class' => 'form-control','placeholder'=>'Enter name'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'name'])
</div>
<div class="form-group">
        {{ Form::label('icon', null, ['class' => 'control-label']) }}
        {{ Form::text('icon', null, (['class' => 'form-control','placeholder'=>'Enter icon name'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'icon'])
</div>
<div class="form-group">
    {{ Form::label('url', null, ['class' => 'control-label']) }}
    {{ Form::text('url', null, (['class' => 'form-control','placeholder'=>'Enter url'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'url'])
</div>
<div class="form-group">
        {{ Form::label('rank', null, ['class' => 'control-label']) }}
        {{ Form::number('rank', null, (['class' => 'form-control','placeholder'=>'Enter rank'])) }}
    @include('backend.includes.form_fields_validation',['fieldname' => 'rank'])
</div>

