
<div class="form-group">
    {{ Form::label('Status', null, ['class' => 'control-label ']) }}<br>
    {{ Form::radio('status', '1', ['class' => 'form-control'])  }} Active
    {{ Form::radio('status', '0', true)  }} De Active
        @include('backend.includes.form_fields_validation',['fieldname' => 'status'])

</div>