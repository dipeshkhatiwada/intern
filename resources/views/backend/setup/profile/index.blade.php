@extends('backend.layouts.app')
@section('title','List of '.$panel)
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{ucfirst($panel)}} Management


            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                @if(isset($panel))<li><a href="{{route($base_route.'.index' )}}">{{ucfirst($panel)}}</a></li>@endif
                <li class="active">Detail</li>
            </ol>
        </section>


    @include('backend.includes.message')



    <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">List of  {{$panel}}</h3>
                </div>
                <div class="box-body table table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Heading</th>
                                <th>Data</th>
                                <th>Action</th>

                            </tr>
                            </thead>

                            <tbody>
                            @php($i=1)
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Name</th>
                                <td>{{$data['rows']->name}}</td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id]]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::text('name', null, ['id' => 'name']) }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>logo</th>
                                <td><img height="100" width="150" src="{{asset('images/profile/'.$data['rows']->logo)}}" alt="{{$data['rows']->logo}}"></td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id],'files'=>true]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::file('file') }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Phone Number</th>
                                <td>{{$data['rows']->phone_number}}</td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id]]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::text('phone_number', null, ['id' => 'phone_number']) }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>

                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Email</th>
                                <td>{{$data['rows']->email}}</td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id]]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::text('email', null, ['id' => 'email']) }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Address</th>
                                <td>{{$data['rows']->address}}</td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id]]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::text('address', null, ['id' => 'address']) }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Facebook link</th>
                                <td><a href="{{$data['rows']->facebook}}" target="_blank">{{$data['rows']->facebook}}</a></td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id]]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::text('facebook', null, ['id' => 'facebook']) }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Twitter Link</th>
                                <td><a href="{{$data['rows']->twitter}}" target="_blank">{{$data['rows']->twitter}}</a></td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id]]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::text('twitter', null, ['id' => 'twitter']) }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Google+ Link</th>
                                <td><a href="{{$data['rows']->google}}" target="_blank">{{$data['rows']->google}}</a></td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id]]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::text('facebook', null, ['id' => 'facebook']) }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Youtube Link</th>
                                <td><a href="{{$data['rows']->youtube}}" target="_blank">{{$data['rows']->youtube}}</a></td>
                                <td>
                                    {!! Form::model($data['rows'], ['route' => [$base_route.'.update', $data['rows']->id]]) !!}
                                    {{ Form::hidden('_method', 'PUT') }}
                                    {{ Form::text('youtube', null, ['id' => 'youtube']) }}
                                    {!! Form::submit('Update !',['class'=> 'bg bg-primary']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ $i++}}</td>
                                <th>Last Updated By</th>
                                <td>{{Auth::user()->where('id', '=', $data['rows']->updated_by)->value('name')}}<br/>({{$data['rows']->updated_at}})</td>
                            </tr>
                            </tbody>

                            <tfoot>
                            <tr>
                                <th>S.N</th>
                                <th>Heading</th>
                                <th>Data</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>

                        </table>
                    </div>


            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>

@endsection

@section('js')
    @include('backend.includes.datatable')
@endsection
