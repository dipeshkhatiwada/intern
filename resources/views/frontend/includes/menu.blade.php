
<div class="header-nav animate-dropdown">
    <div class="container">
        <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="nav-bg-class">
                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                    <div class="nav-outer">
                        <ul class="nav navbar-nav">
                            <li class="active dropdown yamm-fw"> <a href="{{route('frontend.index')}}" data-hover="dropdown" class="dropdown-toggle">Home</a> </li>
                            @foreach($categories as $category)
                                {{--{{dd()}}--}}
                            <li class="dropdown">
                                <a href="" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown">
                                    {{$category->name}}
                                    @if(!empty($category->tag->name))
                                    <span class="menu-label new-menu hidden-xs">{{$category->tag->name}}</span>
                                        @endif
                                </a>
                                <ul class="dropdown-menu ">
                                    <li>
                                        <ul class="links">
                                            @foreach($category->subcategories()->select('id','name','slug')->where('status', '=', 1)->limit(5)->get() as $subcategory)
                                            <li><h2 class="title"><a href="{{ route('frontend.sub-category', ['slug' => $subcategory->slug]) }}">{{$subcategory->name}}</a></h2></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            @endforeach


                            <li class="dropdown"> <a href="contact.html">Some Tag</a> </li>

                            <li class="dropdown  navbar-right special-menu"> <a href="#">Todays offer</a> </li>
                        </ul>
                        <!-- /.navbar-nav -->
                        <div class="clearfix"></div>
                    </div>
                    <!-- /.nav-outer -->
                </div>
                <!-- /.navbar-collapse -->

            </div>
            <!-- /.nav-bg-class -->
        </div>
        <!-- /.navbar-default -->
    </div>
    <!-- /.container-class -->

</div>