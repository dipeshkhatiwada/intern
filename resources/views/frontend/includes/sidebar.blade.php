
<div class="col-xs-12 col-sm-12 col-md-3 sidebar">
    <div class="sidebar-module-container">
        <div class="sidebar-filter">
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
            <div class="sidebar-widget wow fadeInUp">
                <h3 class="section-title">shop by</h3>
                <div class="widget-header">
                    <h4 class="widget-title">Category</h4>
                </div>
                <div class="sidebar-widget-body">
                    <!-- /.accordion-group -->
                    <div class="accordion">
                        @foreach($categories as $category)
                        <div class="accordion-group">
                            <div class="accordion-heading"> <a href="#{{$category->slug}}" data-toggle="collapse" class="accordion-toggle collapsed"> {{$category->name}} </a> </div>
                            <!-- /.accordion-heading -->
                            <div class="accordion-body collapse" id="{{$category->slug}}" style="height: 0px;">
                                <div class="accordion-inner">
                                    <ul>
                                        @foreach($category->subcategories()->select('id','name','slug')->where('status', '=', 1)->limit(5)->get() as $subcategory)
                                            <li><a href="{{ route('frontend.sub-category', ['slug' => $subcategory->slug]) }}">{{$subcategory->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <!-- /.accordion-inner -->
                            </div>
                            <!-- /.accordion-body -->
                        </div>
                        @endforeach




                    </div>
                    <!-- /.accordion -->
                </div>
                <!-- /.sidebar-widget-body -->
            </div>
            <!-- /.sidebar-widget -->
             <div class="sidebar-widget wow fadeInUp">
             <div class="sidebar-widget wow fadeInUp">
                <div class="widget-header">
                    <h4 class="widget-title">Price Slider</h4>
                </div>
                <div class="sidebar-widget-body m-t-10">
                    <div class="price-range-holder"> <span class="min-max"> <span class="pull-left">$200.00</span> <span class="pull-right">$800.00</span> </span>
                        <input type="text" id="amount" style="border:0; color:#666666; font-weight:bold;text-align:center;">
                        <input type="text" class="price-slider" value="" >
                    </div>
                    <!-- /.price-range-holder -->
                    <a href="#" class="lnk btn btn-primary">Show Now</a> </div>
                <!-- /.sidebar-widget-body -->
            </div>
            <!-- tags-->
                 <div class="sidebar-widget product-tag wow fadeInUp">
                     <h3 class="section-title">Product tags</h3>
                     <div class="sidebar-widget-body outer-top-xs">
                         <div class="tag-list">
                             @foreach($tags as $tag)
                                 <a class="item" title="Phone" href="category.html">{{$tag->name}}</a>
                             @endforeach
                             <a class="item active" title="Vest" href="category.html">Vest</a>

                         </div>
                         <!-- /.tag-list -->
                     </div>
                     <!-- /.sidebar-widget-body -->
                 </div>

            <div class="home-banner"> </div>
        </div>
        <!-- /.sidebar-filter -->
    </div>

    <div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs">
        <h3 class="section-title">hot deals</h3>
        <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss">
            <div class="item">
                <div class="products">
                    <div class="hot-deal-wrapper">
                        <div class="image"> <img src="{{asset('frontend/images/hot-deals/p25.jpg')}}" alt=""> </div>
                        <div class="sale-offer-tag"><span>49%<br>
                    off</span></div>
                        <div class="timing-wrapper">
                            <div class="box-wrapper">
                                <div class="date box"> <span class="key">120</span> <span class="value">DAYS</span> </div>
                            </div>
                            <div class="box-wrapper">
                                <div class="hour box"> <span class="key">20</span> <span class="value">HRS</span> </div>
                            </div>
                            <div class="box-wrapper">
                                <div class="minutes box"> <span class="key">36</span> <span class="value">MINS</span> </div>
                            </div>
                            <div class="box-wrapper hidden-md">
                                <div class="seconds box"> <span class="key">60</span> <span class="value">SEC</span> </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.hot-deal-wrapper -->

                    <div class="product-info text-left m-t-20">
                        <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                        <div class="rating rateit-small"></div>
                        <div class="product-price"> <span class="price"> $600.00 </span> <span class="price-before-discount">$800.00</span> </div>
                        <!-- /.product-price -->

                    </div>
                    <!-- /.product-info -->

                    <div class="cart clearfix animate-effect">
                        <div class="action">
                            <div class="add-cart-button btn-group">
                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i class="fa fa-shopping-cart"></i> </button>
                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                            </div>
                        </div>
                        <!-- /.action -->
                    </div>
                    <!-- /.cart -->
                </div>
            </div>
            <div class="item">
                <div class="products">
                    <div class="hot-deal-wrapper">
                        <div class="image"> <img src="{{asset('frontend/images/hot-deals/p5.jpg')}}" alt=""> </div>
                        <div class="sale-offer-tag"><span>35%<br>
                    off</span></div>
                        <div class="timing-wrapper">
                            <div class="box-wrapper">
                                <div class="date box"> <span class="key">120</span> <span class="value">Days</span> </div>
                            </div>
                            <div class="box-wrapper">
                                <div class="hour box"> <span class="key">20</span> <span class="value">HRS</span> </div>
                            </div>
                            <div class="box-wrapper">
                                <div class="minutes box"> <span class="key">36</span> <span class="value">MINS</span> </div>
                            </div>
                            <div class="box-wrapper hidden-md">
                                <div class="seconds box"> <span class="key">60</span> <span class="value">SEC</span> </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.hot-deal-wrapper -->

                    <div class="product-info text-left m-t-20">
                        <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                        <div class="rating rateit-small"></div>
                        <div class="product-price"> <span class="price"> $600.00 </span> <span class="price-before-discount">$800.00</span> </div>
                        <!-- /.product-price -->

                    </div>
                    <!-- /.product-info -->

                    <div class="cart clearfix animate-effect">
                        <div class="action">
                            <div class="add-cart-button btn-group">
                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i class="fa fa-shopping-cart"></i> </button>
                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                            </div>
                        </div>
                        <!-- /.action -->
                    </div>
                    <!-- /.cart -->
                </div>
            </div>
            <div class="item">
                <div class="products">
                    <div class="hot-deal-wrapper">
                        <div class="image"> <img src="{{asset('frontend/images/hot-deals/p10.jpg')}}" alt=""> </div>
                        <div class="sale-offer-tag"><span>35%<br>
                    off</span></div>
                        <div class="timing-wrapper">
                            <div class="box-wrapper">
                                <div class="date box"> <span class="key">120</span> <span class="value">Days</span> </div>
                            </div>
                            <div class="box-wrapper">
                                <div class="hour box"> <span class="key">20</span> <span class="value">HRS</span> </div>
                            </div>
                            <div class="box-wrapper">
                                <div class="minutes box"> <span class="key">36</span> <span class="value">MINS</span> </div>
                            </div>
                            <div class="box-wrapper hidden-md">
                                <div class="seconds box"> <span class="key">60</span> <span class="value">SEC</span> </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.hot-deal-wrapper -->

                    <div class="product-info text-left m-t-20">
                        <h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>
                        <div class="rating rateit-small"></div>
                        <div class="product-price"> <span class="price"> $600.00 </span> <span class="price-before-discount">$800.00</span> </div>
                        <!-- /.product-price -->

                    </div>
                    <!-- /.product-info -->

                    <div class="cart clearfix animate-effect">
                        <div class="action">
                            <div class="add-cart-button btn-group">
                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button"> <i class="fa fa-shopping-cart"></i> </button>
                                <button class="btn btn-primary cart-btn" type="button">Add to cart</button>
                            </div>
                        </div>
                        <!-- /.action -->
                    </div>
                    <!-- /.cart -->
                </div>
            </div>
        </div>
        <!-- /.sidebar-widget -->
    </div>

    <!-- compaire -->
        <div class="sidebar-widget wow fadeInUp outer-top-vs">
            <h3 class="section-title">Compare products</h3>
            <div class="sidebar-widget-body">
                <div class="compare-report">
                    <p>You have no <span>item(s)</span> to compare</p>
                </div>
                <!-- /.compare-report -->
            </div>
            <!-- /.sidebar-widget-body -->
        </div>

    <div class="home-banner">   </div>
</div>
