<!-- ============================================== HEADER ============================================== -->
<header class="header-style-0">
    <div class="top-bar animate-dropdown">
        <div class="container">
            <div class="header-top-inner">

                <div class="cnt-account">

                    <ul class="list-unstyled">
                        <!-- <li><a href="#"><i class="icon fa fa-user"></i>My Account</a></li> -->
                        <li><a href="#"><i class="icon fa fa-heart"></i>Wishlist</a></li>
                        <li><a href="#"><i class="icon fa fa-signal"></i>compare</a></li>
                        <li><a href="#"><i class="icon fa fa-check"></i>Checkout</a></li>

                        {{--<li><a href="#"> <i class="icon fa fa-facebook"></i>.</a></li>--}}
                        {{--<li><a href="#">  <i class="icon fa fa-youtube"></i>.</a></li>--}}
                        {{--<li><a href="#"> <i class="icon fa fa-twitter"></i>.</a></li>--}}
                        <!-- <li><a href="#"><i class="icon fa fa-lock"></i>Login</a></li> -->
                    </ul>



                </div>

                <div class="clearfix"></div>

            </div>
            <!-- /.header-top-inner -->
        </div>
        <!-- /.container -->
    </div>
</header>
<!-- /.header-top -->
<header class="header-style-1">

    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-2 logo-holder">
                    <!-- ============================================================= LOGO ============================================================= -->
                    <div class="logo"> <a href="home.html"> <img src="{{asset('frontend/images/logo.png')}}" alt="logo"> </a> </div>
                    <!-- /.logo -->
                    <!-- ============================================================= LOGO : END ============================================================= -->
                </div>
                <!-- /.logo-holder -->

                <div class="col-xs-12 col-sm-12 col-md-5 top-search-holder">
                    <div class="search-area">
                        <form>
                            <div class="control-group">

                                <input class="search-field" placeholder="Search here..." />
                                <a  class="search-button" href="#" ></a> </div>
                        </form>
                    </div>
                </div>
                <!-- /.top-search-holder -->

                <!-- SHOPPING CART DROPDOWN -->
                <div class="col-xs-5 col-sm-5 col-md-3 animate-dropdown top-cart-row">
                    @guest('customer')
                    <div class=" dropdown-cart">
                        <a href="{{ route('customer.register') }}" class="dropdown-toggle lnk-cart" >
                            <div class="items-cart-inner">
                                <div class="basket">
                                    <i class="fa fa-user"></i> &nbsp; My Account
                                </div>
                            </div>
                        </a>
                    </div>
                @else
                    <div class="dropdown dropdown-cart">
                        <a href="" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
                            <div class="items-cart-inner">
                                <div class="basket">
                                    <i class="fa fa-user"></i> &nbsp;{{ \Illuminate\Support\Facades\Auth::guard('customer')->user()->name }}
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="clearfix cart-total">


                                    <a href="{{route('customer.dashboard')}}" class="btn btn-upper btn-primary btn-block m-t-20"><i class="fa fa-user"></i> &nbsp; Profile</a>
                                    <a href="{{ route('customer.auth.logout') }}" class="btn btn-upper btn-danger btn-block m-t-20"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        &nbsp;  <i class="fa fa-sign-out"></i>&nbsp; Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('customer.auth.logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div><!-- /.cart-total-->


                            </li>
                        </ul>
                    </div>

                @endguest
                    <!-- /.dropdown-cart -->
                </div>
                <!--  SHOPPING CART DROPDOWN : END -->

                <!-- SHOPPING CART DROPDOWN -->
                <div class="col-xs-7 col-sm-7 col-md-2 animate-dropdown top-cart-row">

{{--{{dd(\Gloudemans\Shoppingcart\Facades\Cart::content())}}--}}
                    <div class="dropdown dropdown-cart"> <a href="{{route('frontend.listcart')}}" class="dropdown-toggle lnk-cart">
                            <div class="items-cart-inner">
                                <div class="basket"> <i class="glyphicon glyphicon-shopping-cart"></i> </div>
                                <div class="basket-item-count"><span class="count">{{\Gloudemans\Shoppingcart\Facades\Cart::count()}}</span></div>
                                <div class="total-price-basket"> <span class="lbl"> -</span> <span class="total-price"> <span class="sign">Rs.</span><span class="value">{{\Gloudemans\Shoppingcart\Facades\Cart::subtotal()}}</span> </span> </div>
                            </div>
                        </a>

                        <!-- /.dropdown-menu-->
                    </div>
                    <!-- /.dropdown-cart -->
                </div>
                <!--  SHOPPING CART DROPDOWN : END -->


                <!-- /.top-cart-row -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

    </div>
    <!-- /.main-header -->
@include('frontend.includes.menu')
</header>

