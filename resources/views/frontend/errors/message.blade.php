@extends('frontend.layouts.master')

@section('content')

<div class="body-content outer-top-bd">
    <div class="container">
        <div class="x-page inner-bottom-sm">
            <div class="row">
                <div class="col-md-12 x-text text-center">
                    <h1>{{$code}} Error</h1>
                    <h3><i class="fa fa-warning text-yellow"></i> Oops! {{!empty($message)?$message:'There was some error.'}} </h3>
                    <a href="home.html"><i class="fa fa-home"></i> Go To Homepage</a>
                </div>
            </div><!-- /.row -->
        </div><!-- /.sigin-in-->
    </div><!-- /.container -->
</div><!-- /.body-content -->
@endsection

