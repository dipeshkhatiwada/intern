<!DOCTYPE html>
<html lang="en">
@include('frontend.includes.head')
<body class="cnt-home">
@include('frontend.includes.header')
@yield('content')
<!-- /#top-banner-and-menu -->
<!-- move to top button -->


@include('frontend.includes.footer')

<!-- For demo purposes – can be removed on production -->

<!-- For demo purposes – can be removed on production : End -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="{{asset('frontend/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap-hover-dropdown.min.js')}}"></script>
<script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontend/js/echo.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.easing-1.3.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.rateit.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/lightbox.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('frontend/js/wow.min.js')}}"></script>
<script src="{{asset('frontend/js/scripts.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>
@yield('js')

</body>
</html>
