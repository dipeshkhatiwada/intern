@extends('backend.layouts.app')
@section('title','Update Password ')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Change Password</h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>

                <li class="active">Change Password</li>
            </ol>
        </section>
        @include('backend.includes.message')
        <section class="content">
            @include('backend.includes.errors')
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$data['page_title']}}</h3>
                </div>

                {!! Form::open(['route' => 'changePassword','method'=>'POST']) !!}

                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('current-password', 'Current Password', ['class' => 'control-label']) }}
                        {{ Form::password('current-password', ['class' => 'awesome form-control','placeholder'=>'Enter Current Password']) }}
                        @include('backend.includes.form_fields_validation',['fieldname' => 'current-password'])
                    </div>
                    <div class="form-group">
                        {{ Form::label('new-password', 'New Password', ['class' => 'control-label']) }}
                        {{ Form::password('new-password', ['class' => 'awesome form-control','placeholder'=>'Enter New Password']) }}
                        @include('backend.includes.form_fields_validation',['fieldname' => 'new-password'])
                    </div>
                    <div class="form-group">
                        {{ Form::label('new-password_confirmation', 'Confirm New Password', ['class' => 'control-label']) }}
                        {{ Form::password('new-password_confirmation', ['class' => 'awesome form-control','placeholder'=>'Enter Confirm New Password']) }}
                        @include('backend.includes.form_fields_validation',['fieldname' => 'new-password_confirmation'])

                    </div>


                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check icheck"></i> Change Password ! </button>
                    <button type="reset" class="btn btn-warning"><i class="fa fa-undo icheck"></i> Cancel</button>
                </div>
                <!-- /.box-footer-->
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@endsection

@section('js')
    @include('backend.includes.ckeditor')
    {{--@include('backend.includes.slug')--}}
@endsection
