<?php

namespace App\Http\Controllers\frontend;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer',['only' => 'index','edit']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.customer.dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.customer.index');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'name'          => 'required',
            'email'         => 'required',
            'phone'         => 'required',
            'address'         => 'required',
            'password'      => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6'
        ]);
        // store in the database
        $customers = new Customer();
        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->phone = $request->phone;
        $customers->address = $request->address;
        $customers->verification_key = uniqid();
        $customers->status = 0;
        $customers->last_login = date('Y-m-d H:i:s');
        $customers->password=bcrypt($request->password);
        if (  $customers->save()){
            $path = route('customer.register.verify',$customers->verification_key);
            $link = "<a href='$path' target='_blank'>Verify</a>";
            $request->session()->flash('success_message',"Your Registration is Success!, Please click on verification link, $link");
        }else {
            $request->session()->flash('error_message','Registration Failed!');
            return redirect()->route('customer.auth.login');
        }
        return redirect()->route('customer.auth.login');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    function  verification(Request $request,$code)
    {

        $customer = Customer::where('verification_key' ,$code)->where('status',0)->get();
//        dd($customer);
        if (count($customer) == 1){
            $customer[0]->verification_key = '';
            $customer[0]->status = 1;
            $customer[0]->email_verified_at=date('Y-m-d H:i:s');
            $customer[0]->update();
            $request->session()->flash('success_message','Verification Success!, Please Login');
            return redirect()->route('customer.auth.login');

        }else {
            $request->session()->flash('error_message','Verification Failed!, Please Try Again');
            return redirect()->route('customer.auth.login');
        }
    }

    protected function guard()
    {
        return Auth::guard('customer');
    }


}
