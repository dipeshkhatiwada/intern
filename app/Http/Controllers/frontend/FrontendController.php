<?php

namespace App\Http\Controllers\frontend;


use App\Models\Product;
use App\Models\Profile;
use App\Models\Slider;

use App\Models\Subcategory;
use Gloudemans\Shoppingcart\Facades\Cart;
use Gloudemans\Shoppingcart\Contracts\InstanceIdentifier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    function index(){

        $data['page_title']='Home';
        $data['slider'] = Slider::select('id','name','title','slug','image')
            ->where('status', '=', 1)
            ->orderBy('rank','ASC')
            ->limit(5)
            ->get();
        $data['product'] = Product::select('id','name','price','slug','discount')
            ->where([['status', '=', 1],['feature_key', '=', 1]])
            ->orderBy('created_at','DESC')
            ->limit(5)
            ->get();
//        dd($data['product']);

//        $data['tag'] = Tag::where('status', '=', 1)->orderBy('created_at','DESC')->limit(11)->get();
//
//        $data['main_news'] = Post::where([['status', '=', 1],['main_news', '=', 1]])
//            ->orderBy('created_at','DESC')->limit(10)->get();
//        $data['categories'] = Category::where('status', '=', 1)->orderBy('rank','ASC')->get();

        return view('frontend.home.index',compact('data'));
    }
    function subcategory ($slug){

        $data['subcategory'] = Subcategory::select('id','name','description','image')
            ->where('slug',$slug)
            ->first();

        $data['product']= Subcategory::where('slug', $slug)
            ->first()
            ->products()
            ->where('status', '=', 1)
            ->select('id','name','price','slug','discount')
            ->orderBy('created_at','DESC')
            ->paginate(9);
//        dd($data);

        return view('frontend.subcategory.index',compact('data'));
    }

    function product($slug){

        $data['product'] = Product::select('id','name','price','slug','discount','stock','short_description','description')
            ->where('slug',$slug)
            ->first();
        $data['image']=$data['product']->product_images()
            ->select('name')
            ->where('status', '=', 1)
            ->orderBy('created_at','DESC')
            ->get();
        $data['attributes']=$data['product']
            ->product_attributes()
            ->select('name','value')
            ->where('status', '=', 1)
            ->orderBy('created_at','DESC')
            ->get();

        return view('frontend.product.index',compact('data'));
    }

    function  addtocart(Request $request){

        Cart::add(['id' => $request->input('id'), 'name' => $request->input('name'), 'qty' => $request->input('quantity'), 'price' => $request->input('amount'),'weight' => $request->input('amount')]);
//        return back();
        $data['carts'] = Cart::content();
        $data['page_title'] = 'Cart List';
        return view('frontend.cart.index',compact('data'));
    }

    function  listcart(){
        $data['carts'] = Cart::content();
//        dd($data['carts']);
//        $data['title'] = 'Cart List';
        return view('frontend.cart.index',compact('data'));

    }

    function  deletecart($rowId){
        Cart::remove($rowId);
        return back();

    }
    function  checkout(){
        $data['carts'] = Cart::content();
        return view('frontend.cart.checkout',compact('data'));
    }


    public function contact  ()
    {
        $data = [];
        $this->panel='Contact Us';
        $this->base_route='';
        $data['configuration']=Profile::orderBy('created_at','DESC')
            ->first();
//     dd($data['configuration']);
        return view('frontend.contact-us.index', compact('data'));
    }

    public function store (ContactRequest $request)
    {
        Message::create($request->all());
//        dd($request);

        $request->session()->flash($this->success_message,  'Message Successfully Saved !');
        return redirect()->route('frontend.contact-us');

    }


}
