<?php

namespace App\Http\Controllers\frontend;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PasswordResetController extends Controller
{

    function forgot(){
        return view('frontend.customer.forgot');
    }
    function  sendMail(Request $request){
        $this->validate($request, ['email' => 'required|email']);
        $customer = Customer::where('email',$request->input('email'))->get();
        if(count($customer) == 1){
            $customer[0]->reset_token = uniqid();
            $customer[0]->update();
            $customer = $customer[0];
            $link = url('/') . '/customer/reset=' . $customer->reset_token;
            /*Mail::to($request->input('email'))
                ->send(new ForgotEmail($request));*/
            Mail::send('frontend.customer.forgotemail_format', ['email' => $request->input('email'),'link' => $link], function ($m) use ($customer,$link) {
                $m->from('info@test.com', 'Your Passwrd Reset Link');
                $m->to($customer->email, $customer->name)->subject('Your Password Reset Link!');
            });
            $request->session()->flash('success_message','Reset link sent into email,please check your email!!');
            return redirect()->back();
        }else{
            $request->session()->flash('error_message','Customer email not match Failed!!');
            return redirect()->back();
        }
    }
    function  reset(Request $request,$code){
        $customer = Customer::where('reset_token',$code)->get();
        if (count($customer) == 1){
            return view('frontend.customer.reset',compact('customer'));
        } else {
            $request->session()->flash('error_message','Invalid Reset Token!!');
            return redirect()->back();
        }

    }
    function resetpassword(Request $request){
        $this->validate($request, [
            'password' => 'required|confirmed',
        ]);
        $customer = Customer::where('reset_token',$request->input('reset_code'))->get();

        if(count($customer) == 1){
            $customer[0]->reset_token = '';
            $customer[0]->password = bcrypt($request->input('password'));
            $customer[0]->update();

            $request->session()->flash('success_message','Your password has been changed, Please login to continue');
            return redirect()->route('customer.register');
        }else{
            $request->session()->flash('error_message','Password Update Failed!!');
            return redirect()->back();
        }
    }

}
