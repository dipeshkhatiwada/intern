<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\backend\SliderRequest;
use App\Http\Requests\backend\SubcategoryRequest;
use App\Models\Category;
use App\Models\Slider;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class SlidersController extends BackendBaseController{

    protected $base_route = 'backend.slider';
//    protected $view_path = 'backend.slider';
    protected $panel = 'slider';
    protected $folder_path;
//    protected $folder_name = 'slider';
//    protected $trans_path = 'backend/social_security/test_base/general.';


    public function __construct (){
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->panel . DIRECTORY_SEPARATOR;
    }
    public function index (){
        $data = [];
        $data['rows'] = Slider::orderBy('created_at','DESC')->get();
        return view($this->loadDataToView($this->base_route . '.index'), compact('data'));
    }
    public function create (){
        $data = [];
        return view($this->loadDataToView($this->base_route . '.create'), compact('data'));
    }


    public function store(SliderRequest $request){

        if ($request->hasFile('photo')){
            $file_name = $this->UploadFiles($request->file('photo'));
            $request->request->add(['image' => $file_name]);
        }
        $request->request->add(['created_by' => auth()->user()->id]);
        $store=Slider::create($request->all());
        if ($store){
            $request->session()->flash($this->success_message, ucfirst($this->panel) . ' Successfully Created !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error,ucfirst($this->panel) . ' cann\'t be Created  ! ');
            return back();
        }
    }

    public function show($id){
        $data = [];
        if (!$data['row'] = Slider::find($id))
            return parent::invalidRequest();
        return view(parent::loadDataToView($this->base_route . '.show'), compact('data'));

    }
    public function edit($id){
        $data = [];
        if (!$data['row'] = Slider::find($id))
            return parent::invalidRequest();
        return view(parent::loadDataToView($this->base_route . '.edit'), compact('data'));
    }


    public function update(SliderRequest $request, $id){
        if (!$row = Slider::find($id)) {
            $request->session()->flash('message_error', 'Invalid Request !');
            return redirect()->route($this->base_route.'.index');
        }
        if ($request->hasFile('photo')) {

            $file_name = $this->UploadFiles($request->file('photo'));
            if (file_exists($this->folder_path . $row->image)){
                unlink($this->folder_path . $row->image);
                $this->UnlinkImage($row);
            }

        }
        $request->request->add(['image' => isset($file_name) ? $file_name : $row->image]);
        $request->request->add(['updated_by' => Auth::user()->id]);
        $row->update($request->all());

        if ($row){
            $request->session()->flash($this->success_message, ucfirst($this->panel) . ' Successfully updated !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error, ucfirst($this->panel) . ' update failed  ! ');
            return back();
        }
    }
    public function destroy(Request $request,$id){
        if (!$row = Slider::find($id)) {
            $request->session()->flash($this->message_error, ' Invalid Request  ! ');
            return redirect()->route($this->base_route.'.index');
        }
        if (file_exists($this->folder_path . $row->image)){
            unlink($this->folder_path . $row->image);
            $this->UnlinkImage($row);
        }
        $row->delete();
        if ($row){
            $request->session()->flash($this->message_error, ucfirst($this->panel) . ' Successfully deleted !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->success_message, ucfirst($this->panel) . ' delete failed  ! ');
            return redirect()->route($this->base_route.'.index');
        }

    }
    protected function UploadFiles($image)
    {
        //  $image      = $request->file('photo');
        $image_name = rand(6785, 9814).'_'.$image->getClientOriginalName();
        $image->move($this->folder_path, $image_name);
        //code for image resize
        foreach (config('image.image_dimensions.slider.image') as $dimension) {
            // open and resize an image file
            $img = Image::make($this->folder_path.$image_name)->resize($dimension['width'], $dimension['height']);
            // save the same file as jpg with default quality
            $img->save($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$image_name);
        }

        return $image_name;
    }
    protected function UnlinkImage($row)
    {
        foreach (config('image.image_dimensions.slider.image') as $dimension) {
        //     dd($dimension);
            if (file_exists($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_' . $row->image))
                unlink($this->folder_path .$dimension['width'].'_'.$dimension['height'].'_'. $row->image);
        }


    }
}
