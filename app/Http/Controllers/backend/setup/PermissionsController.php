<?php

namespace App\Http\Controllers\backend\setup;

use App\Http\Controllers\Backend\BackendBaseController;
use App\Http\Requests\backend\PermissionRequest;

use App\Models\Module;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionsController extends BackendBaseController
{
    protected $base_route = 'backend.setup.permission';
    protected $view_path = 'backend.setup.permission';
    protected $panel = 'permission';
    protected $folder_path;
    protected $folder_name = 'permission';
    protected $trans_path = '';


    public function __construct (){
        $this->file_path = public_path() . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $this->folder_name . DIRECTORY_SEPARATOR;
    }
    public function index (){
        $data = [];
        $data['rows'] = Permission::orderBy('created_at','DESC')->get();
        return view($this->loadDataToView($this->view_path . '.index'), compact('data'));
    }

    public function create (){
        $data = [];
        $data['modules']=Module::pluck('name','id');
//        $data['modules']->prepend('Select modules');
        return view($this->loadDataToView($this->view_path . '.create'), compact('data'));
    }

    public function store(PermissionRequest $request)
    {
        $request->request->add(['created_by' => Auth::user()->id]);
        $status=Permission::create($request->all());
        if($status){
            $request->session()->flash('success_message','Permission created Successfully  ! ');
            return redirect()->route('backend.permission.index');
        }else{
            $request->session()->flash('error_messge','Premission cann\'t be created  ! ');
            return back();
        }
    }



    public function show($id)
    {
        $data = [];
        if (!$data['row'] = Permission::find($id))
            return parent::invalidRequest();
        return view(parent::loadDataToView($this->base_route . '.show'), compact('data'));
    }

    public function edit($id){
        $data = [];
        if (!$data['row'] = Permission::find($id))
            return parent::invalidRequest();
        $data['base_route'] = $this->base_route;
        $data['modules']=Module::pluck('name','id');

        return view(parent::loadDataToView($this->view_path . '.edit'), compact('data'));
    }

    public function update(Request $request, $id)
    {
        if (!$row = Permission::find($id)) {
            $request->session()->flash('message_error', 'Invalid Request !');
            return redirect()->route($this->base_route.'.index');
        }
        $request->request->add(['updated_by' => Auth::user()->id]);
        $row->update($request->all());
        if ($row){
            $request->session()->flash($this->success_message, $this->panel . ' Successfully updated !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error, $this->panel . ' update failed  ! ');
            return back();
        }
    }

    public function destroy(Request $request,$id)
    {
        if (!$row = Permission::find($id)) {
            $request->session()->flash($this->message_error, ' Invalid Request  ! ');
            return redirect()->route($this->base_route.'.index');
        }
        $row->delete();
        $request->session()->flash($this->message_error, $this->panel . ' Successfully deleted !');
        return redirect()->route($this->base_route.'.index');
    }
}
