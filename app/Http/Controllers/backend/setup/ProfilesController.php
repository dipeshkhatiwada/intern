<?php

namespace App\Http\Controllers\backend\setup;

use App\Http\Controllers\Backend\BackendBaseController;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ProfilesController extends BackendBaseController {
    protected $base_route = 'backend.setup.profile';
    protected $view_path = 'backend.setup.profile';
    protected $panel = 'profile';
    protected $folder_path;
    protected $folder_name = 'profile';
    protected $trans_path = 'backend/social_security/test_base/general.';


    public function __construct (){
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->folder_name . DIRECTORY_SEPARATOR;
    }
    public function index (){
        $data = [];
        $data['rows'] = Profile::first();
        return view($this->loadDataToView($this->view_path . '.index'), compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        $data['page_title']='Create Profile';
//        $data['link']='profile';
//
//        return view('backend.profiles.create',compact('data'));
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(CreateProfileRequest $request)
//    {
//        $request->request->add(['created_by'=> Auth::user()->id]);
//        $profile=Profile::create($request->all());
//        if ($profile){
//            $request->session()->flash('success_message','Profile created Successfully  ! ');
//            return redirect()->route('backend.profile.index');
//        }else{
//            $request->session()->flash('error_messge','Profile cann\'t be Created  ! ');
//            return back();
//        }
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//        $data['page_title']='Edit Profile';
//        $data['profile']=Profile::find($id);
//        $data['link']='profile';
//
//        return view('backend.profiles.show',compact('data'));
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        $data['page_title']='Edit Profile';
//        $data['profile']=Profile::find($id);
//        $data['link']='profile';
//        return view('backend.profiles.edit',compact('data'));
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$row = Profile::find($id)) {
            $request->session()->flash('message_error', 'Invalid Request !');
            return redirect()->route($this->base_route.'.index');
        }
        if ($request->hasFile('file')) {

            $file_name = $this->UploadFiles($request);
    //            if (file_exists($this->file_path . $row->image))
    //                unlink($this->file_path . $row->image);
        }
        $request->request->add(['logo' => isset($file_name) ? $file_name : $row->logo]);
        $row->update($request->all());

        if ($row){
            $request->session()->flash($this->success_message, $this->panel . ' Successfully updated !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error, $this->panel . ' update failed  ! ');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function destroy($id)
//    {
//        $profile= Profile::find($id);
//        $profile->delete();
//        return redirect()->route('backend.profile.index');
//
//    }
    protected function Uploadfiles(Request $request)
    {
        $image      = $request->file('file');
        $image_name = rand(6785, 9814).'_'.$image->getClientOriginalName();
        $image->move($this->folder_path, $image_name);
//        dd($this->folder_path);
        //code for image resize
        foreach (config('image.image_dimensions.news.image') as $dimension) {
            // open and resize an image file
            $img = Image::make($this->folder_path.$image_name)->resize($dimension['width'], $dimension['height']);
            // save the same file as jpg with default quality
            $img->save($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$image_name);
        }

        return $image_name;
    }
}
