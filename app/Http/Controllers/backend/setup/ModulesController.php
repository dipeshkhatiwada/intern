<?php

namespace App\Http\Controllers\backend\setup;

use App\Http\Controllers\Backend\BackendBaseController;
use App\Http\Requests\backend\ModuleRequest;
use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModulesController extends BackendBaseController
{
    protected $base_route = 'backend.setup.module';
    protected $view_path = 'backend.setup.module';
    protected $panel = 'module';
    protected $folder_path;
    protected $folder_name = 'module';
    protected $trans_path = '';


    public function __construct (){
        $this->file_path = public_path() . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $this->folder_name . DIRECTORY_SEPARATOR;
    }
    public function index (){
        $data = [];
        $data['rows'] = Module::orderBy('created_at','DESC')->get();
        return view($this->loadDataToView($this->view_path . '.index'), compact('data'));
    }

    public function create (){
        $data = [];
        return view($this->loadDataToView($this->view_path . '.create'), compact('data'));
    }

    public function store(ModuleRequest $request)
    {
        $request->request->add(['created_by' => Auth::user()->id]);
        $status=Module::create($request->all());
        if($status){
            $request->session()->flash('success_message','Module created Successfully  ! ');
            return redirect()->route('backend.module.index');
        }else{
            $request->session()->flash('error_messge','Module cann\'t be Created  ! ');
            return back();
        }
    }

    public function show($id)
    {
        $data = [];
        if (!$data['row'] = Module::find($id))
            return parent::invalidRequest();
        return view(parent::loadDataToView($this->base_route . '.show'), compact('data'));
    }


    public function edit($id){
        $data = [];
        if (!$data['row'] = Module::find($id))
            return parent::invalidRequest();
        $data['base_route'] = $this->base_route;
        return view(parent::loadDataToView($this->view_path . '.edit'), compact('data'));
    }


    public function update(Request $request, $id)
    {
        if (!$row = Module::find($id)) {
            $request->session()->flash('message_error', 'Invalid Request !');
            return redirect()->route($this->base_route.'.index');
        }
        $row->update($request->all());

        if ($row){
            $request->session()->flash($this->success_message, $this->panel . ' Successfully updated !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error, $this->panel . ' update failed  ! ');
            return back();
        }

    }

    public function destroy(Request $request,$id){
        if (!$row = Module::find($id)) {
            $request->session()->flash($this->message_error, ' Invalid Request  ! ');
            return redirect()->route($this->base_route.'.index');
        }
        $row->delete();
        return redirect()->route($this->base_route.'.index');

    }
}
