<?php
namespace App\Http\Controllers\backend\setup;

use App\Http\Controllers\Backend\BackendBaseController;
use App\Http\Requests\backend\RoleRequest;
use App\Models\Module;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RolesController extends BackendBaseController {
    protected $base_route = 'backend.setup.role';
    protected $view_path = 'backend.setup.role';
    protected $panel = 'role';
    protected $folder_path;
    protected $folder_name = 'role';
    protected $trans_path = '';


    public function __construct (){
        $this->file_path = public_path() . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $this->folder_name . DIRECTORY_SEPARATOR;
    }
    public function index (){
        $data = [];
        $data['rows'] =Role::orderBy('created_at','DESC')->get();
        return view($this->loadDataToView($this->view_path . '.index'), compact('data'));
    }

    public function create (){
        $data = [];
        return view($this->loadDataToView($this->view_path . '.create'), compact('data'));
    }

    public function store(RoleRequest $request){
        $request->request->add(['created_by' => auth()->user()->id]);
        $store=Role::create($request->all());
        if ($store){
            $request->session()->flash($this->success_message, $this->panel . ' Successfully Created !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error, $this->panel . ' cann\'t be Created  ! ');
            return back();
        }
    }
    public function show($id)
    {
        $data = [];
        if (!$data['row'] = Role::find($id))
            return parent::invalidRequest();
//        $data['base_route'] = $this->base_route;
        return view(parent::loadDataToView($this->view_path .'.show'), compact('data'));
    }
    public function edit($id){
        $data = [];
        if (!$data['row'] = Role::find($id))
            return parent::invalidRequest();
        $data['base_route'] = $this->base_route;
        return view(parent::loadDataToView($this->view_path . '.edit'), compact('data'));
    }
    public function update(RoleRequest $request, $id){
        if (!$row = Role::find($id)) {
            $request->session()->flash('message_error', 'Invalid Request !');
            return redirect()->route($this->base_route.'.index');
        }$request->request->add(['updated_by' => Auth::user()->id]);
        $row->update($request->all());

        if ($row){
            $request->session()->flash($this->success_message, $this->panel . ' Successfully updated !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error, $this->panel . ' update failed  ! ');
            return back();
        }
    }
    public function destroy($id)
    {
        $Role=Role::find($id);
        $Role->delete();
        return redirect()->route('backend.role.index');
    }

    function  assignpermission($id){
        $data['page_title']="Assign Permission to  Role";
        $data['link']='role';
        $data['role'] = Role::find($id);
        $data['permissions'] = Permission::where('status',1)->get();
        $ap = [];
        foreach ($data['role']->permissions as $permission){
            $ap[] = $permission->id;
        }
        $data['assigned_permissions'] = $ap;
        return view(parent::loadDataToView($this->view_path .'.assignpermission'), compact('data'));
    }

    function  postpermission(Request $request){
        $role = Role::find($request->input('role_id'));
        $role->permissions()->sync($request->input('permission_id'));
        $request->session()->flash($this->success_message, ' Permission assigned to Role Successfully  !');
        return redirect()->route($this->base_route.'.index');

    }

    function  assignmodule($id){
        $data['page_title']="Assign Module to  Role";
        $data['link']='role';
        $data['role'] = Role::find($id);
        $data['modules'] = Module::where('status',1)->orderby('rank')->get();
        $am = [];
        foreach ($data['role']->modules as $module){
            $am[] = $module->id;
        }
        $data['assigned_modules'] = $am;
        return view(parent::loadDataToView($this->view_path .'.assignmodule'), compact('data'));
    }

    function  postmodule(Request $request){
        $role = Role::find($request->input('role_id'));
        $role->modules()->sync($request->input('module_id'));
        $request->session()->flash('success_message','Module assigned to Role Successfully  ! ');
        return redirect()->route($this->base_route.'.index');



    }
}
