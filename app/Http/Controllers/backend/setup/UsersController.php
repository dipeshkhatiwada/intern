<?php

namespace App\Http\Controllers\backend\setup;

use App\Http\Controllers\Backend\BackendBaseController;
use App\Http\Requests\backend\UserRequest;
use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class UsersController extends BackendBaseController {

    protected $base_route = 'backend.setup.user';
    protected $view_path = 'backend.setup.user';
    protected $panel = 'user';
    protected $folder_path;
    protected $folder_name = 'users';
    protected $trans_path = 'backend/social_security/test_base/general.';


    public function __construct (){
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->folder_name . DIRECTORY_SEPARATOR;
    }
    public function index (){
        $data = [];
        $data['rows'] = User::orderBy('created_at','DESC')->get();
        return view($this->loadDataToView($this->view_path . '.index'), compact('data'));
    }

    public function create (){
        $data = [];
        $data['roles']=Role::pluck('name','id');
        $data['roles']->prepend('Select role');
        return view($this->loadDataToView($this->view_path . '.create'), compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if ($request->hasFile('photo')){
            $file_name = $this->UploadFiles($request);
            $request->request->add(['image' => $file_name]);
        }
       $request->request->add(['password'=> Hash::make($request->input('password'))]);
        $store=User::create($request->all());
        if ($store){
            $request->session()->flash($this->success_message, $this->panel . ' Successfully Created !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error, $this->panel . ' cann\'t be Created  ! ');
            return back();
        }

    }

    public function show($id)
    {
        $data = [];
        if (!$data['row'] = User::find($id))
            return parent::invalidRequest();
        return view(parent::loadDataToView($this->base_route . '.show'), compact('data'));
    }
    public function edit($id)
    {
        $data['roles']=Role::pluck('name','id');
        $data['page_title']='Edit User';
        $data['user']=User::find($id);
        $data['link']='user';
        return view('backend.user.edit',compact('data'));
    }

    public function update(Request $request, $id)
    {
        if ($request->has('file')){
            $file = $request->file('file');
            $file_name=uniqid().'_'.$file->getClientOriginalName();
            $file->move('images/users',$file_name);
            $request->request->add(['image'=> $file_name]);
        }
        $user= User::find($id);
        if ($request->input('password') != $user->password)
            $request->request->add(['password'=> Hash::make($request->input('password'))]);

        $user->update($request->all());
        if ($user){
            $request->session()->flash('success_message','User updated Successfully  ! ');
            return redirect()->route('backend.user.index');
        }else{
            $request->session()->flash('error_messge','User cann\'t be updated  ! ');
            return back();
        }
    }


    public function destroy($id)
    {
        $user= User::find($id);
        $user->delete();
        return redirect()->route('backend.user.index');

    }
    protected function UploadFiles(Request $request)
    {
        $image      = $request->file('photo');
        $image_name = rand(6785, 9814).'_'.$image->getClientOriginalName();
        $image->move($this->folder_path, $image_name);
//        dd($this->folder_path);
        //code for image resize
        foreach (config('image.image_dimensions.user.image') as $dimension) {
            // open and resize an image file
            $img = Image::make($this->folder_path.$image_name)->resize($dimension['width'], $dimension['height']);
            // save the same file as jpg with default quality
            $img->save($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$image_name);
        }

        return $image_name;
    }
}
