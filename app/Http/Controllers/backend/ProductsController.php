<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\backend\ProductRequest;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductImage;
use App\Models\Subcategory;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class ProductsController extends BackendBaseController{

    protected $base_route = 'backend.product';
    protected $panel = 'product';
    protected $folder_path;


    public function __construct (){
        $this->folder_path = public_path() . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->panel . DIRECTORY_SEPARATOR;
    }
    public function index (){
        $data = [];
        $data['rows'] = Product::orderBy('created_at','DESC')->get();
        return view($this->loadDataToView($this->base_route . '.index'), compact('data'));
    }
    public function create (){
        $data = [];
        $data['tags']=Tag::where([['status', '=', 1],['hot_key', '=', 0]])
            ->pluck('name','id');
        $data['categories']=Subcategory::pluck('name','id');
        return view($this->loadDataToView($this->base_route . '.create'), compact('data'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $request->request->add(['created_by'=> Auth::user()->id]);
        $request->request->add(['stock'=> $request->input('quantity')]);
        $request->request->add(['view_count'=> 0]);
        $store=Product::create($request->all());
//        dd($request);

        if ($store){
            $store->tags()->sync($request->input('tag_id'));
            for($i=0;$i<count($request->input('attribute_name'));$i++){
                $att= [
                    'product_id' =>   $store->id,
                    'name' =>   $request->input('attribute_name')[$i],
                    'value' =>$request->input('attribute_value')[$i],
                    'status' => 1,
                    'created_by' =>Auth::user()->id,
                ];
                ProductAttribute::create($att);
            }

            if ($request->has('photo')){
                foreach ($request->file('photo') as $photo){
                    $file_name = $this->UploadFiles($photo);
                    $request->request->add(['image' => $file_name]);

                    $request->request->add(['image'=> $file_name]);
                    $image=[
                        'name' => $file_name,
                        'product_id' => $store->id,
                        'status' => 1,
                        'created_by' =>Auth::user()->id,
                    ];
                    ProductImage::create($image);

                }
            }
//            dd($request);
            $request->session()->flash($this->success_message, ucfirst($this->panel) . ' Successfully Created !');
            return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash($this->message_error,ucfirst($this->panel) . ' cann\'t be Created  ! ');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!$data['row'] = Product::find($id))
            return parent::invalidRequest();
        $data['images']=ProductImage::where('product_id',$data['row']->id)->get();
        $data['tags']=[];

        return view(parent::loadDataToView($this->base_route . '.show'), compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tags']=Tag::where([['status', '=', 1],['hot_key', '=', 0]])
            ->pluck('name','id');
        $data['page_title']='Edit Product';
        $data['product']=Product::find($id);
        $data['categories']=Category::pluck('name','id');

        $data['categories']->prepend('Select Category','');
//        dd($data);
        return view('backend.products.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product= Product::find($id);
        $product->update($request->all());
        if ($product){
            $product->tags()->sync($request->input('tag_id'));
            if ($request->has('file')){
                foreach ($request->file('file') as $file){
                    $file_name=uniqid().'_'.$file->getClientOriginalName();
                    $file->move('images/products',$file_name);
                    $request->request->add(['image'=> $file_name]);
                    $image=[
                        'name' => $file_name,
                        'product_id' => $product->id,
                        'created_by' =>Auth::user()->id,
                    ];
                    ProductImage::create($image);
                }
            }

            for($i=0;$i<count($request->input('attribute_name'));$i++){
                if (isset($request->input('attribute_id')[$i]) && !empty($request->input('attribute_id')[$i])){
                   $product_attribute = ProductAttribute::find($request->input('attribute_id')[$i]);
                    $product_attribute->name =    $request->input('attribute_name')[$i];
                    $product_attribute->value =    $request->input('attribute_value')[$i];
                    $product_attribute->updated_by = Auth::user()->id;
                    $product_attribute->update();

                }else{
                    if (!empty($request->input('attribute_name')[$i])) {

                        $att = [
                            'product_id' => $id,
                            'name' => $request->input('attribute_name')[$i],
                            'value' => $request->input('attribute_value')[$i],
                            'created_by' => Auth::user()->id,
                        ];
                        ProductAttribute::create($att);
                    }
                }

            }

            return redirect()->route('backend.product.index');
        }else{
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd('dsvhjkl');
//        $product= Product::find($id);
//        $product->delete();
//        return redirect()->route('backend.product.index');
    }
    function deleteimage(Request $request){
//        dd('ducj');
        $product_image= ProductImage::find($_POST['image_id']);
        if( $product_image->delete()){
            return 'true';
        }else{
            return 'false';
        }

    }
    protected function UploadFiles($image)
    {
        //  $image      = $request->file('photo');
        $image_name = rand(6785, 9814).'_'.$image->getClientOriginalName();
        $image->move($this->folder_path, $image_name);
        //code for image resize
        foreach (config('image.image_dimensions.slider.image') as $dimension) {
            // open and resize an image file
            $img = Image::make($this->folder_path.$image_name)->resize($dimension['width'], $dimension['height']);
            // save the same file as jpg with default quality
            $img->save($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_'.$image_name);
        }

        return $image_name;
    }
    protected function UnlinkImage($row)
    {
        foreach (config('image.image_dimensions.slider.image') as $dimension) {
            //     dd($dimension);
            if (file_exists($this->folder_path.$dimension['width'].'_'.$dimension['height'].'_' . $row->image))
                unlink($this->folder_path .$dimension['width'].'_'.$dimension['height'].'_'. $row->image);
        }


    }
}
