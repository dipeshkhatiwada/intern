<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class SubcategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=>'required',
            'name' => 'required|unique:subcategories'. (request()->method()==="POST"?'':',name,'.$this->id),
            'slug' => 'required| unique:subcategories'. (request()->method()=="POST" ? '': ',slug,'.$this->id),
            'rank'=>'required|integer',
        ];
    }
    function messages()
    {
        return [
            'required' => 'Please Enter :attribute .',
            'unique' => ':attribute must be unique .',
            'category_id.required'=>'Please Select category .',


        ];
    }
}
