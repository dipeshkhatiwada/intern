<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'password' => ((request()->method() == 'POST') ? 'required' : 'nullable').'|confirmed|min:3',
            'email'    => 'required |unique:users'.(request()->method() == "POST" ? '' : ',email,'.$this->id),
            'photo'=>'required',
        ];
    }
    function messages()
    {
        return [
            'required' => 'Please Enter :attribute .',
            'unique' => ':attribute must be unique .',
            'photo.required'=>'Please Select Image',
        ];
    }
}
