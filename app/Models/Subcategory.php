<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table='subcategories';
    protected $fillable=['category_id','name','slug','rank','status','image','description','meta_keywords','meta_description','meta_title','created_by','updated_by'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
