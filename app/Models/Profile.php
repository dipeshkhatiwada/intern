<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table='profiles';
    protected $fillable=['name','logo','phone_number','email','address'
        ,'facebook','twitter','google','youtube'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
