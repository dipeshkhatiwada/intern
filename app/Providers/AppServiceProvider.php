<?php

namespace App\Providers;


use App\Models\Category;
use App\Models\Profile;
use App\Models\Tag;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use \Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer(['frontend.includes.menu'], function ($view) {
            $view
                ->with('categories', Category::select('id','name','slug','tag_id')
                    ->where([['status', '=', 1],['menu_display', '=', 1]])
                    ->orderBy('rank','ASC')
                    ->limit(5)
                    ->get()
                );
        });
        View::composer(['frontend.includes.sidebar'], function ($view) {
            $view
                ->with('categories', Category::select('id','name','slug')
                ->where([['status', '=', 1],['menu_display', '=', 1]])
                ->orderBy('rank','ASC')
                ->limit(10)
                ->get()
                )
                ->with('tags', Tag::select('name','slug')
                    ->where([['status', '=', 1],['hot_key', '=', 0]])
                    ->orderBy('created_at','DESC')
                    ->limit(7)
                    ->get()
                );

        });
        View::composer(['frontend.includes.footer'], function ($view) {
            $view
                ->with('profile', Profile::first());
        });

//        View::composer(['frontend.includes.header','frontend.includes.add','frontend.includes.footer'], function ($view) {
//
//            $profile = Profile::orderBy('created_at','DESC')->first();
//
//            $view->with('profiles', $profile);
////            $view->with('advertisements', $advertisement);
//
//        });

//        View::composer(['frontend.includes.header'], function ($view) {
//            $categories = Category::where([
//                ['status', '=', 1],
//                ['menu_display', '=', 1]
//            ])->orderBy('rank','ASC')->limit(8)->get();
//            $view->with('categories', $categories);
//
//        });
//
//        View::composer(['frontend.includes.footer'], function ($view) {
//            $categories = Category::where('status', '=', 1)->orderBy('rank','ASC')->limit(12)->get();
//            $view->with('categories', $categories);
//
//        });
//
//        View::composer('frontend.includes.footer', function ($view) {
//            $tags = Tag::where('status',1)->orderby('name')->get();
//            $view->with('tags', $tags);
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
