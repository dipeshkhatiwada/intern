<?php

    Auth::routes();
    Route::get('/changePassword',       'HomeController@showChangePasswordForm');
    Route::get('/home',                 'HomeController@index')             ->name('home');
    Route::post('/changePassword',      'HomeController@changePassword')    ->name('changePassword');
    Route::get('{id}/profile',          'HomeController@profile')           ->name('profile');
    Route::put('{id}/profile',          'HomeController@updateProfile')     ->name('profile.update');
    Route::post('payment/paywithpaypal', 'Frontend\PaymentController@payWithpaypal')->name('payment.paywithpaypal');
    Route::get('payment/paymentstatus', 'Frontend\PaymentController@getPaymentStatus')->name('paypal.paymentstatus');
    Route::get('payment/successpayment', 'Frontend\PaymentController@successpayment')->name('paypal.successpayment');

//----customer controller--//
    Route::prefix('customer')->namespace('frontend')->group(function () {
        Route::get('/',                         'CustomerController@index')                 ->name('customer.dashboard');
        Route::get('dashboard',                 'CustomerController@index')                 ->name('customer.dashboard');
        Route::get('account.html',              'CustomerController@create')                ->name('customer.register');
        Route::post('register',                 'CustomerController@store')                 ->name('customer.register.store');
        Route::get('key={code}',                'CustomerController@verification')          ->name('customer.register.verify');
        Route::get('forgot.html',               'PasswordResetController@forgot')           ->name('customer.forgot.password');
        Route::post('forgot.html',              'PasswordResetController@sendMail')         ->name('customer.forgot.send_reset');
        Route::get('reset={token}',             'PasswordResetController@reset')            ->name('customer.reset');
        Route::post('resetpassword',            'PasswordResetController@resetpassword')    ->name('customer.reset_password');



    });
    //----customerLogin controller--//
    Route::prefix('customer')->group(function () {
        Route::get('login',       'Auth\CustomerLoginController@login')             ->name('customer.auth.login');
        Route::post('login',      'Auth\CustomerLoginController@loginCustomer')     ->name('customer.auth.loginCustomer');
        Route::post('logout',     'Auth\CustomerLoginController@logout')            ->name('customer.auth.logout');
    });


    Route::namespace('frontend')->name('frontend.')->group(function () {
        Route::get('/newsletter',               'NewsletterController@create')      ->name('newsletter.create');
        Route::post('/newsletter',              'NewsletterController@store')       ->name('newsletter.store');

        Route::post('contact-us/store',         'FrontendController@store')         ->name('contact-us.store');
        Route::get('contact-us.html',           'FrontendController@contact')       ->name('contact-us.create');


        //-----cart----//
        Route::post('/cart/add',                'FrontendController@addtocart')     ->name('addtocart');
        Route::get('/cart.html',                'FrontendController@listcart')      ->name('listcart');
        Route::get('/cart/delete/{rowId}',      'FrontendController@deletecart')    ->name('deletecart');
        Route::get('/cart/checkout.html',       'FrontendController@checkout')      ->name('checkout');

        Route::get('/',                         'FrontendController@index')          ->name('index');
        Route::get('/product/{slug}',           'FrontendController@product')        ->name('product');
        Route::get('/post/{slug}',              'FrontendController@post')           ->name('post');
        Route::get('/{slug}',                   'FrontendController@subcategory')    ->name('sub-category');



        Route::get('/tag', 'FrontendController@tag')->name('tag');
//            Route::post('/post', 'CommentsController@store')->name('store');
            Route::get('/cart/delete/{rowId}', 'FrontendController@deletecart')->name('deletecart');
    });

    //--------Backend route define-----------//

    Route::prefix('backend')->middleware(['web','auth','checkpermission'])->namespace('backend')->name('backend.')->group(function (){

        Route::post('product/deleteimage',                       'ProductsController@deleteimage')->name('product.deleteimage');

        Route::resource('category',         'CategoriesController');
        Route::resource('subcategory',      'SubcategoriesController');
        Route::resource('advertisement',    'AdvertisementsController');
//        Route::resource('comment',          'CommentsController');
        Route::resource('slider',          'SlidersController');

     //--------Product group-------------//
        Route::resource('tag',              'TagsController');
        Route::resource('product',             'ProductsController');



    //---------Single define---------//
        Route::post('/subcategory/getDataByCategory_id',    'SubcategoriesController@getDataByCategory_id')     ->name('subcategory.getDataByCategory_id');
    });
Route::group(['prefix' => 'backend/setup/', 'as' => 'backend.setup.', 'namespace' => 'backend\setup\\', 'middleware' => ['web','auth','checkpermission']], function (){


        //---------- Basic setup----------------//

        Route::resource('user',             'UsersController');
        Route::resource('role',             'RolesController');
        Route::resource('permission',       'PermissionsController');
        Route::resource('module',           'ModulesController');
        Route::resource('profile',          'ProfilesController');


    Route::get('role/assignpermission/{id}',           'RolesController@assignpermission')                 ->name('role.assignpermission');
    Route::post('role/assignpermission',               'RolesController@postpermission')                   ->name('role.postpermission');
    Route::get('role/assignmodule/{id}',               'RolesController@assignmodule')                     ->name('role.assignmodule');
    Route::post('role/assignmodule',                   'RolesController@postmodule')                       ->name('role.postmodule');


});
